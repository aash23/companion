---
layout: post
title:  "One Year Anniversary"
description: Gratitude for and Celebration of 1 Year Wedding Anniversary
categories: wedding
---
![couple]({{site.baseurl}}/images/couple.jpg)

Hi Hema,

A year has gone by since we tied the knot!

I remember the first time meeting you. All I thought of was to be myself around you, enjoy every moment with you, and make sure that you are comfortable and at ease with me. I am glad that I was able to do all that around you. It is not because I willed it but primarily because you made it so easy for me. I hadn’t been so comfortable and at ease with anyone till then. You had such an amiable personality that it was almost effortless to connect with you.

And, I think the same attribute of yours has been one of the strong anchors in our relationship. It is very easy for me to confide in you my deepest thoughts, opinions, fears and everything else (that I can’t think of right now.) In addition, you have been a wonderful partner (obviously :-P), friend, custodian and critique.

Our marriage started with the onset of Covid in our lives. We took the adversity head on, supported each other in every way and I think, the event, though unfortunate, brought us closer to each other and to each other’s family. We then proceeded to Mumbai to build our own home. Finding the place, setting up everything, managing household, etc we have done all together.

In spite of having different personalities, divergent tastes, etc we managed to tackle all the minor hiccups in our lives fearlessly and amicably. We both showed immense commitment in our relationship. As a result or in spite of all this, I think our love for each other has blossomed and reached new heights. I can proudly and happily say that I now don’t just love you but depend on you.

You have been the nucleus in our relationship. Your maturity in tackling difficult situations, your honesty and sincerity in the relationship, your sense of and commitment to equality, your act of providing loving concern and care, your unconditional love, your unadulterated self-belief, your steadfast determination in achieving your milestones, your immensely helpful and unbiased rational side, your funny / entertaining antics at times - all these qualities have served as a strong foundation in our happy marriage.

Hema, I am lucky to have you.

***Here comes the cliche line.***

And I am glad that marriage is a commitment of 7 lives because one life would be too less of a time to spend and enjoy with you.

Thanks for being you and I can’t wait to spend the rest of my life with you







