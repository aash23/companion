---
layout: post
title:  "4 Year Anniversary"
description: Celebrating 4 Year marriage anniversary.
categories: wedding
---


![couple]({{site.baseurl}}/images/anniver.jpeg)

Hi Hema,

Happy 4 year anniversary!!

Whenever I think of having to write a few words on our marriage from my perspective, immediately the first thing that strikes my mind is how incredibly grateful I am for having you in my life. At a time when I had lost almost all hope of finding true love, you came into my world casually and our relationship gradually blossomed into the tenet of marriage. It was cupid in the true sense for me.

As the years passed by, we have both grown more mature in the relationship. The intense struggle of knowing, understanding and adjusting with each other, especially during COVID when we were together 24 hours of the day,  gave us a crash course on the impact of marriages in both of our lives. In hindsight I realize that that period was one of the crucial few years of our marriage as it brought out what is really important for both of us - love, respect and commitment. 

I have learnt so much from you over the years. Your clear and straightforward way of communicating your thoughts, feelings and opinions, your display of emotional maturity while dealing with intense situations, your presence of mind while diffusing flammable circumstances, your way of accommodating everyone’s tastes and opinions while planning or preparing for events, your true and unbiased love for the everyone in the family, your commitment towards consistently improving ourselves in marriage, et cetera - are all the qualities in you that I really admire, respect and am taking small steps to observe and learn.

In addition, I think, as the years passed by, we have projected a bit of ourselves on each other in varied ways - both for you and me. Talking more vs. talking less,  going out vs. staying in., eating spicy food vs non. spicy food, adopting a healthy lifestyle vs. consistently exercising, being emotionally intelligent vs. becoming more objective etc. are some of the examples where we have grown on each other. 

Thanks a lot hema for staying with me through all the thick and thin that we have been through. Your commitment towards making the relationship work is impeccable. Every moment is new, interesting and exciting for me when I am with you. Your smile lights up my day in innumerable ways. Your happiness is of prime importance to me.  

I am excitedly looking forward to spending the rest of my life with you.

Thank you for being there in my life,

Aash

